; Programa FIRST.asm
INICIO EQU 0H ;as in "define"
			ORG INICIO ;allocates code starting from the address argument
			MOV 02H,#3 ;# means actual data, not an address
VOLTA: 		MOV A,R2 ;02H = R2
			ADD A,32H ;adds and save on the acc
			MOV 32H,A
			JMP VOLTA ;sums the next instruction to the lsb of the lastest instruction (goes to 3)
			END ;not instruction, ends the cycle of running
