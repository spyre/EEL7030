;primeiro exercício: clp ex0
;se mudar a interrupção, só alterar a porta e o endereço
reset equ 00h
ltint0 equ 13h ; local tratador [tabelado]
state equ 20h

	org reset ;PC=0 depois de reset
	jmp inicio

	org ltint0
	jmp handler ;use of jmp bc only 8bytes in the handler register, so you jump to other place

inicio:
	mov ie,#10000100b ; habilita interrupção [tabelado]
	setb it1 ; borda [tabelado]
	mov state,#0h ;inicialização
	mov r0,#state ;r0 <= 20h
	mov dptr,#tabela ;datapointer aponta para tabela de caracteres
	mov r1,#0 ;usado para indexar a tabela

volta:
	;cjne -> compare jump (if) not equal
	cjne @r0,#1,volta ;compare indirect with immediate if not jmp to volta
	mov state,#0h
	mov a,r1
	movc a,@a+dptr
	mov p1,a
	inc r1
	cjne r1,#16,volta
	jmp $

handler: 
	mov state,#1h
	reti ;carrega 00 no msb do pc, carrega 14 no lsb

tabela: db 'Microcontrolador' ;macroassembler interpreta ascii
end