;Programa ADDVECT.asm
RESET EQU 0H
VETOR EQU 60H
ORG RESET ; PC = 0000H ao se resetar o 8051
		MOV DPTR,#NRO ; endereco nro parcelas a ser somado
		MOV A,#0
		MOVC A,@A+DPTR ;useful to read from ROM, sums A + DPTR and points to A (@ indirect pointing)
		JZ FIM ;if 0 goes to argument
		MOV R1,A ; R1 = nro parcelas a ser somado
		MOV DPTR,#DADOS ; end. vetor de dados a ser somado
		MOV R3,#0 ; keep carry flag bit
		MOV R2,#0 ; guarda resultado das somas realizadas
		MOV R0,#0 ; especifica parcela a ser lida do vetor de dados
VOLTA:  MOV A,R0 ;A goes to 0
		MOVC A,@A+DPTR ; 0 index number of DADOS goes to acc (how?)
		ADD A,R2
		MOV R2,A
		MOV A,#0
		ADDC A,R3 ;adds A + R3 + carry flag bit, R3 = zero now (keeps in A?)
		MOV R3, A
		INC R0
		DJNZ R1,VOLTA ; conditional return cause
		MOV DPTR, #1H ; data pointer poiting to that address of memory
		MOVX @DPTR, A
FIM: JMP FIM ;same as JMP $ as flag
ORG VETOR ;alocates all the following program starting from argument, if ommited it just allocates wherever
NRO: DB 0H ;define byte, allocates into rom; number of numbers in vector
DADOS: DB 01H,03H,05H,06H,0AH,0E2H
END
