--------------------------------------------------------------------------------
;EXERCÍCIO 7.1: RECEBE DADOS E ENCHE A MEMÓRIA DO ENDEREÇO 30H ATÉ 35H CICLICAMENTE
--------------------------------------------------------------------------------
RESET 	    EQU 00H
LTSERIAL    EQU 23H
STATE  	    EQU 20H
	
	ORG RESET
	JMP INICIO
	
	ORG LTSERIAL 
	CLR RI
	SETB F0
	RETI
	
INICIO:
		
	MOV 		IE,#10010010B
	MOV	    	SCON, #01010000B
	MOV 		PCON, #0H
	MOV 		TMOD,#00100001b ; TEMPORIZADOR 1 NO MODO 2 E TEMPORIZADOR 0 NO MODO 1
	MOV 		TH1,#0FDH
	MOV 		TL1,#0FDH
	MOV	    	R0, #030H
	SETB 		TR1
	
VOLTA:
	JNB			F0, VOLTA
	CLR			F0
	MOV		    @R0, SBUF
	INC			R0
	CJNE		R0, #35H, VOLTA
	CLR 		TR1
	MOV		    R0, #30H
	JMP 		VOLTA

END


--------------------------------------------------------------------------------
7.2 TRANSMITE DE 41H ATÉ WHATEVER COM BIT DE PARIDADE, 65KNPS OU COISA PARECIDA
--------------------------------------------------------------------------------
RESET 				EQU 00H
LTSERIAL 		    EQU 23H 
STATE 				EQU 20H

ORG 					RESET  
JMP 					INICIO 

ORG 					LTSERIAL
SETB					F0
CLR 					TI
RETI

INICIO:  
		MOV 		IE,#10010000B
		MOV     	SCON,#11000000B
		MOV 		TMOD,#00100000B
		MOV			PCON, #080H
		MOV      	TH1,#0FFH
		MOV      	TL1,#0FFH
		SETB 		TR1
		MOV			A, #41H
		MOV 		C, P
		MOV			TB8, C
	    MOV			SBUF, A
		INC 		A
VOLTA: 
		JNB			F0, VOLTA
		CLR			F0
		MOV 	    C, P
		MOV			TB8, C
	    MOV			SBUF, A
		INC 		A
		CJNE		A, #80H, VOLTA
		MOV 		A, #41H
		JMP			VOLTA
		
		MOV SBUF, R1
		
	
END
--------------------------------------------------------------------------------
7.3 - ENVIA E RECEBE TRECOS
--------------------------------------------------------------------------------
RESET 				EQU 00H
LTSERIAL 		EQU 23H 
STATE 				EQU 20H
F1						EQU PSW.1

ORG 					RESET  
JMP 					INICIO 

ORG 					LTSERIAL
JNB					RI, LABEL1
CLR					RI
SETB					F0
LABEL1: 			
	JNB					TI, RETURN
	CLR 					TI
	SETB					F1
RETURN: 				RETI

INICIO:  
		MOV 		R1, #30H

		MOV 		IE,#10010000B
		MOV     	SCON,#11010000B
		MOV 		TMOD,#00100000B
		MOV		PCON, #080H
		MOV      	TH1,#0FFH
		MOV      	TL1,#0FFH
		SETB 	TR1
		MOV		A, #41H
		MOV 		C, P
		MOV		TB8, C
	    MOV		SBUF, A
		
RECEBE:
			JNB 	F0, ENVIA
			CLR	F0
			MOV 	@R1, SBUF
			INC 	R1
			CJNE R1, #36H, ENVIA
			MOV	R1, #30H
				
ENVIA: 
		JNB		F1, RECEBE
		CLR		F1
		INC 		A
		MOV 	    C, P
		MOV		TB8, C
	    MOV		SBUF, A
		CJNE		A, #80H, RECEBE
		MOV 		A, #41H
		JMP		RECEBE
		
	
END
-----------------------------------------------------------------------------
7.4 RECEBE/ENVIA E GUARDA NA MEMÓRIA EXTERNA. SHOW DE BOLA, TCHÊ
-----------------------------------------------------------------------------
RESET 				EQU 00H
LTSERIAL 		EQU 23H 
STATE 				EQU 20H
F1						EQU PSW.1

ORG 					RESET  
JMP 					INICIO 

ORG 					LTSERIAL
JNB					RI, LABEL1
CLR					RI
SETB					F0
LABEL1: 			
	JNB					TI, RETURN
	CLR 					TI
	SETB					F1
RETURN: 				RETI

INICIO:  
		MOV 		DPTR, #0H
		MOV		R3, #0H
		MOV 		IE,#10010000B
		MOV     	SCON,#11010000B
		MOV 		TMOD,#00100000B
		MOV		PCON, #080H
		MOV      	TH1,#0FDH
		MOV      	TL1,#0FDH
		SETB 	TR1
		MOV		R5, #41H
		MOV 		C, P
		MOV		TB8, C
	    MOV		SBUF, R5
		
RECEBE:
			JNB 	F0, ENVIA
			CLR	F0
			MOV A, SBUF
			MOVX @DPTR, A
			INC 	R3
			INC DPTR
			CJNE R3, #09H, ENVIA
			MOV	DPTR, #00H
			MOV R3, #0H
				
ENVIA: 
		JNB		F1, RECEBE
		CLR		F1
		INC 		R5
		MOV 	    C, P
		MOV		TB8, C
	    MOV		SBUF, R5
		CJNE		R5, #61H, RECEBE
		MOV 		R5, #41H
		JMP		RECEBE
		
	
END

