ex 6.1
--------------------------------------------------------------------------------
RESET EQU 0H
LTMR0 EQU 0BH ; local do tratador
STATE EQU 20H
ORG RESET ; PC=0 depois de reset
JMP INICIO
ORG LTMR0
MOV TH0, #0FDH
MOV TL0, #080H
MOV STATE, #1H
RETI
INICIO: MOV IE, #10000010B ; habilita TMR0
MOV TMOD, #01H ; modo 1
MOV TH0, #0FDH ; fundo de escala - 640 = (2^16) - 640
MOV TL0, #80H ; 65.536 - 640 = 64.896 = 0FD80H
MOV STATE, #0H ; inicialização1
MOV R0, #STATE
MOV DPTR, #TABELA
MOV R1, #0
SETB TR0
VOLTA: CJNE @R0, #1, VOLTA
MOV STATE, #0H
MOV A, R1
MOVC A, @A+DPTR
MOV P1, A
INC R1
CJNE R1, #16, VOLTA
CLR TR0
JMP $
TABELA: DB "MICROCONTROLADOR"
END



--------------------------------------------------------------------------------
ex 6.2
--------------------------------------------------------------------------------
reset equ 0h
ltmr0 equ 01bh ; local do tratador
state equ 20h

org reset ;PC=0 depois de reset
jmp inicio

org ltmr0
mov tl1,#00h
mov th1,#0ECH
mov state,#1h
reti

inicio:
	mov ie,#10001000b ; EN ALL - TIMER 1
	mov tmod,#00000000b ; modo 0 - contador 13 bits
	mov th1,#0ECH 
	mov tl1,#00h
	mov state,#0h ;inicialização
	mov r0,#state
	mov dptr,#tabela
	mov r1,#0
	setb tr1

volta: 
	cjne @r0,#1,volta
	mov state,#0h
	mov a,r1
	movc a,@a+dptr	
	mov p1,a
	inc r1
	cjne r1,#16,volta
	clr tr1
	jmp $

tabela: db 'Microcontrolador'
end

--------------------------------------------------------------------------------
ex 6.3 -- 5 vezes 128 = 640
--------------------------------------------------------------------------------
reset EQU 0h
ltmr0 EQU 0bh ; local do tratador
state EQU 20h
	
ORG reset ;PC=0 depois de reset
JMP inicio

ORG ltmr0
			MOV state,#1h
RETI

inicio:
	MOV 		ie,#10000010B ; EN ALL - TIMER 0
	MOV 		tmod,#00000010b ; modo 2 - 8 bits com recarga
	MOV 		th0,#80h
	MOV 		tl0,#80h
	MOV 		state,#0h ;inicialização
	MOV 		r0,#state
	MOV	    	dptr,#tabela
	MOV 		r1,#0
	MOV		    r2, #0
	SETB 		tr0

volta: 
	CJNE 		@r0,#1,volta
	MOV 		state,#0h
	INC			r2
	CJNE		r2, #5, volta
	MOV		    r2, #0
	MOV 		a,r1
	MOVC 	    a,@a+dptr
	MOV 		p1,a
	INC 		r1
	CJNE 		r1,#16,volta
	MOV 		r1,#0
	MOV		    r2, #0
	JMP			volta

tabela: db 'Microcontrolador'
end

--------------------------------------------------------------------------------
ex 6.4 esse é topster
--------------------------------------------------------------------------------
reset EQU 0h
ltmr0 EQU 0bh ; local do tratador
ltmr1 EQU 01BH
state EQU 20h
	
ORG reset ;PC=0 depois de reset
JMP inicio

ORG ltmr0
			MOV state,#1h
RETI

ORG ltmr1
	CPL P2.3
	MOV 		th1,#0FCh
	MOV 		tl1,#03Fh
RETI


inicio:
	MOV 		ie,#10001010B ; EN ALL - TIMER 1 - TIMER 0
	MOV 		tmod,#00000010b ; TIMER0 com recarga, TIMER 1 com 16 bits
	MOV 		th0,#80h
	MOV 		tl0,#80h
	MOV 		th1,#0FCh
	MOV 		tl1,#03Fh
	MOV 		state,#0h ;inicialização
	MOV 		r0,#state
	MOV		dptr,#tabela
	MOV 		r1,#0
	MOV		r2, #0
	MOV		P2, #00H
	SETB 		tr0
	SETB 		tr1

volta: 
	CJNE 		@r0,#1,volta
	MOV 		state,#0h
	INC			r2
	CJNE		r2, #5, volta
	MOV		r2, #0
	MOV 		a,r1
	MOVC 	a,@a+dptr
	MOV 		p1,a
	INC 			r1
	CJNE 		r1,#16,volta
	MOV 		r1,#0
	MOV		r2, #0
	JMP			volta

tabela: db 'Microcontrolador'
end
--------------------------------------------------------------------------------
ex 6.5
--------------------------------------------------------------------------------
reset EQU 0h
ltmr0 EQU 0bh ; LOCAL TRATADOR TIMER 0
state EQU 20h
LTINT1 EQU 013H

ORG reset ;PC=0 depois de reset
JMP inicio

ORG ltmr0
			MOV state,#1h
RETI

ORG LTINT1
	JMP HANDLER2
	JMP volta1


inicio:
	MOV 		ie,#10000110B ; EN ALL - TIMER 1 -  INTX 1 - TIMER 0 
	MOV 		tmod,#00000010b ; TIMER0 com recarga
	MOV 		th0,#80h
	MOV 		tl0,#80h
	MOV 		state,#0h ;inicialização
	MOV 		r0,#state
	MOV		dptr,#tabela
	MOV 		r1,#0
	MOV		r2, #0
	CLR			F0
	SETB 		tr0

volta: 
	CJNE 		@r0,#1,volta
	MOV 		state,#0h
	INC			r2
	CJNE		r2, #5, volta
	MOV		r2, #0
	MOV 		a,r1
	MOVC 	a,@a+dptr
	MOV 		p1,a
	INC 			r1
	CJNE 		r1,#16,volta
	MOV 		r1,#0
	MOV		r2, #0
	JB			F0, volta1
	JMP			volta

volta1: 
	CJNE 		@r0,#1,volta1
	MOV 		state,#0h
	MOV 		a,r1
	MOVC 	a,@a+dptr
	MOV 		p1,a
	INC 			r1
	CJNE 		r1,#16,volta1
	MOV 		r1,#0
	MOV		r2, #0
	JMP			volta1

HANDLER2:
	SETB F0
	MOV R3, P2
	MOV TH0, R3
RETI	

tabela: db 'Microcontrolador'
end
--------------------------------------------------------------------------------