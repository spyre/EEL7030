RESET	EQU  00H
LTINT0  EQU  013H ; LOCAL TRATADOR



ORG RESET ;PC=0 DEPOIS DE RESET
	JMP	INICIO

ORG LTINT0
	JMP HANDLER1

	

INICIO:
		MOV IE,#10000100B ;De MSB para LSB: Enable/Disable ALL - Does nothing - Does nothing - Enable/Disable SERIAL - Timer1 - External Interruption 1 - Timer0 - External Interruption 0
		SETB IT0 ;Interrupt Control Bits, se setados, interrupção por borda de descida.
		SETB IT1


LER:	JNB F0, LER
		CLR F0
		MOV A, P1
		MOV R1, A
		MOV A, #0H
		MOV R2, #0H
		MOV R3, #0H
		MOV DPTR, #VALORES

SOMA:
		MOVC A, @A+DPTR
		MOV R3, A
		ADD A, R2
		MOV R2, A
		MOV A, R3
		INC A
		DJNZ R1, SOMA
		MOV P2, R2
		JMP LER


HANDLER1:
		SETB F0
		RETI

VALORES: DB 0H, 01H, 02H, 03H, 04H, 05H, 06H, 07H, 08H, 09H, 0AH, 0BH, 0CH, 0DH, 0EH, 0FH
	END
