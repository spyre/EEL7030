;Template para calls de tabelas em subrotinas usando PC como indexador
ORG 0H
	MOV A, #1 ;numero que você deseja indexar da tabela
	
CHAMADA:
	CALL ROTINA
	
	MOV R1, A
	JMP CHAMADA ;trocar isso por um condicional melhor se realmente for usar

ROTINA:
	INC A ;Caso A for zero
	MOVC A, @A+PC
	RET
	
SUKA:
	DB 01H, 02H, 03H, 04H, 05H
		END