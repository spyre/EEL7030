CS    EQU   P0.7
END0  EQU  P3.3;
END1  EQU  P3.4;

ORG   0h
	CLR   END0 
	CLR   END1
	SETB  CS
	MOV  A,#1
	CALL  CONVERTE
	MOV  P1,A
	JMP   $

;Como essa mazela funciona:
		;A tem que ser pelo menos 1, para não referenciar o próprio RET.
		;Supondo que A = 0, INC A faz A = 1, e nesse caso, o PC será 000F. 
		;O 8051 vai ler a instrução, incrementar o contador (PC = 0010) e adicionar ao acumulador.
		;O endereço referenciado pelo somador + PC serão as instruções não-acessíveis da tabela LOGO ABAIXO da chamada de subrotina.
		;Esse trick não funciona se alocar a tabela em uma parte da memória que esteja LOGO ABAIXO da chamada de return da função.

CONVERTE:
	INC A
	MOVC A,@A+PC
	RET

TABELA: 
	DB 40H, 79H, 24H, 30H, 19H, 12H, 02H, 78H, 00H, 10H, 08H, 03H, 46H, 21H, 06H, 0EH
END