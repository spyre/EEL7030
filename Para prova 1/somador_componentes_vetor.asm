RESET EQU 0H					;Diz que RESET é igual à 0H.
VETOR EQU 60H					;Diz que VETOR é igual à 60H.
ORG RESET 						;Aloca o código a seguir em RESET ou 0H.
		MOV DPTR,#NRO 			;Move a memória onde NRO está alocado para DPTR (06H está na memória).
		MOV A,#0				;Reseta o acumulador
		MOVC A,@A+DPTR 			;Soma ao acumulador o valor do endereço apontado pelo DPTR (nesse caso 06H alocado em 60H).
		JZ FIM 					;Jump if Zero. Pula para _FIM_ se o valor no acumulador é zero.
		MOV R1,A				;Agora R1 possui o valor do número de componentes do vetor.
		MOV DPTR,#DADOS 		;Move o endereço de memória onde DADOS está alocado para o DPTR (0x0061 que contém os componentes dos vetores).
		MOV R2,#0 
		MOV R0,#0 
		
		;O que cada registrador está fazendo: 
			;R0 = Mantendo a contagem ascendente de componentes
			;R1 = Matenndo a contagem descendente de componentes
			;R2 = Servindo de acumulador para adicionar os componentes dos vetores 
			;R3 = Carry.
		
		
VOLTA:  MOV A,R0 				
		MOVC A,@A+DPTR 			;Move para o acumulador o valor do endereço de memória apontado pelo DPTR+A.
		ADD A,R2				;R2 + A.
		MOV R2,A				;R2 = R2 + A.
		MOV A,#0						
		INC R0					
		DJNZ R1,VOLTA 			;Decrement Jump Not Zero. Decrementa o R1, se não for zero vai para VOLTA.
FIM:	JMP FIM

	ORG VETOR 					;Começa a alocar o programa na memória VETOR ou 60H.
NRO: DB 06H;
DADOS: DB 01H,03H,05H,06H,0AH,0E2H

	END
