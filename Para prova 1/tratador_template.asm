RESET EQU 00H
LTEXT0 EQU 03H ; LOCAL TRATADOR
STATE EQU 20H

    ORG RESET
    JMP INICIO
   
    ORG LTEXT0 ;Absoluta melhor tática possível. Nem pense em fazer o handler inteiro a partir de 0003H, seu animal.
    JMP HANDLER
    
INICIO:
    MOV IE,#10000001B ; habilita interrupção [ENABLE ALL e ENABLE EXTERNAL 0]
    MOV TCON,#00000001B ; TIMER CONTROL REGISTER [ENABLE IT0] aka borda de descida da interrupção 0.
    
        MOV STATE,#0H ; inicialização de variável
        MOV R0,# STATE
        MOV DPTR,#TABELA
        MOV R1,#0
        
VOLTA: CJNE @R0,#1,VOLTA
    MOV STATE,#0H
    MOV A,R1
    MOVC A,@A+DPTR
    MOV P1,A
    INC R1
    CJNE R1,#16,VOLTA
    JMP $
    
    
HANDLER: MOV STATE,#1H ; Curto e grosso.
         RETI

TABELA: DB 'Microcontrolador'
 END
